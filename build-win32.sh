#!/bin/bash

set -e

# Preparation
_FLAGS="-DPTW32_STATIC_LIB -Werror"
_ARCH=i686-w64-mingw32
_PREFIX="/usr/${_ARCH}"

source win.env

# Build now
make "$@"
