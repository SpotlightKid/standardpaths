#!/bin/bash

set -e

# Preparation
_FLAGS="-DPTW32_STATIC_LIB -Werror"
_ARCH=x86_64-w64-mingw32
_PREFIX="/usr/${_ARCH}"

source win.env

# Build now
make APP_EXT="-64.exe" "$@"
