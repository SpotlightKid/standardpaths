CC  ?= gcc
CXX ?= g++
TARGET_MACHINE := $(shell $(CC) -dumpmachine)
ifneq ($(LINUX),true)
ifneq ($(MACOS),true)
ifneq ($(WINDOWS),true)

ifneq (,$(findstring linux,$(TARGET_MACHINE)))
LINUX=true
endif
ifneq (,$(findstring apple,$(TARGET_MACHINE)))
MACOS=true
endif
ifneq (,$(findstring mingw,$(TARGET_MACHINE)))
WINDOWS=true
endif
ifneq (,$(findstring windows,$(TARGET_MACHINE)))
WINDOWS=true
endif

endif
endif
endif


CFLAGS += -std=c++17 -I.

ifeq ($(WINDOWS),true)
APP_EXT ?= .exe
CFLAGS += -I./cutf
endif

PROG = teststandardpaths
SOURCES = \
	standardpaths.cpp \
	teststandardpaths.cpp

ifeq ($(WINDOWS),true)
SOURCES += cutf/cutf.cpp
endif

all: $(PROG)$(APP_EXT)

$(PROG)$(APP_EXT): $(SOURCES)
	$(CXX) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	-rm -f $(PROG)$(APP_EXT) *.o
