# standardpaths


A small C++ library to get the system- and user-specfic paths for loading and
storing application data and configuration in a cross-platform and
platform-appropriate way (e.g. using [XDG] on Linux, [SHGetKnownFolderPath]
on Windows, etc.) and taking advantage of the [std::filesystem] API available
since the C++17.


## Installation

Add this repo to your project's repo as a sub-module:

```con
git submodule add https://gitlab.com/0x20.eu/standardpaths
git submodule update --init --recursive
```

## Usage

* Include `"standardpaths.hpp"`.
* Add `standardpaths.cpp` to the list of sources.
* Compile your project with `-std=c++17 -I./standardpaths`.
* Link with `-luuid -lole32` (only needed for Windows targets).
* See [standardpaths.hpp](./standardpaths.hpp) for API.


## Support

For questions, suggestions, or bug reports etc. please use the
[issue tracker](./issues).


## Contributing

Feel free to submit a merge request through Gitlab.


## Authors and Acknowledgments

Created by *Chistopher Arndt*. Inspired by GLib's [gutils.c] and Qt's
[QtStandardPaths].


## License

MIT license


## Project status

This project is in early alpha-stage.


[QtStandardPaths]: https://doc.qt.io/qt-6/qml-qt-labs-platform-standardpaths.html
[SHGetKnownFolderPath]: https://learn.microsoft.com/en-us/windows/win32/api/shlobj_core/nf-shlobj_core-shgetknownfolderpath
[XDG]: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
[gutils.c]: https://gitlab.gnome.org/GNOME/glib/-/blob/5d6c044da45b777970abab2a990208dbeb44794d/glib/gutils.c
[std::filesystem]: https://en.cppreference.com/w/cpp/filesystem
