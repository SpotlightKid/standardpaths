#include <iostream>

#include "standardpaths.hpp"

int main() {
    std::cout << "Home dir = " << getHomeDir() << std::endl;
#ifdef _WIN32
    std::cout << "Windows dir = " << getWindowsDirectory() << std::endl;
    std::cout << "Profile dir = " << getWindowsKnownFolder(FOLDERID_Profile) << std::endl;
#endif
    std::cout << "User config dir = " << getUserConfigDir() << std::endl;
    return 0;
}
