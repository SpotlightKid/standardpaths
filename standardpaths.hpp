#pragma once

#ifdef _WIN32
#include <windows.h>
#include <knownfolders.h>
#include <shlobj.h>

#include "cutf.h"
#else
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#endif

#include <cstdlib>
#include <filesystem>


#ifdef _WIN32
std::filesystem::path getWindowsDirectory();
std::filesystem::path getWindowsKnownFolder(REFKNOWNFOLDERID folder_guid);
#endif
std::filesystem::path getHomeDir();
std::filesystem::path getUserConfigDir();
